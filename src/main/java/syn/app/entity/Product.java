package syn.app.entity;

import java.util.List;

public class Product {
    private int id;
    private String productName;
    private List<GoodsInfo> listGoods;
    private String desc;

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public List<GoodsInfo> getListGoods() {
        return listGoods;
    }

    public void setListGoods(List<GoodsInfo> listGoods) {
        this.listGoods = listGoods;
    }
}
