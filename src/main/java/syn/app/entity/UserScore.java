package syn.app.entity;

import java.io.Serializable;

public class UserScore implements Serializable {
    private  String userName;
    private Double score;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }
}
