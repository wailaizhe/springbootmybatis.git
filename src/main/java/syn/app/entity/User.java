package syn.app.entity;

import jdk.nashorn.internal.objects.annotations.Getter;
import jdk.nashorn.internal.objects.annotations.Setter;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Administrator on 2018\3\11 0011.
 */

public class User implements Serializable {

    private int id;
    private String userName;
    private String password;
    private Long focusNum;
    private boolean focus;//关注 true 取消 false
    private String token;
    List<User> thumbList;//点赞列表

    public List<User> getThumbList() {
        return thumbList;
    }

    public void setThumbList(List<User> thumbList) {
        this.thumbList = thumbList;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isFocus() {
        return focus;
    }

    public void setFocus(boolean focus) {
        this.focus = focus;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Long getFocusNum() {
        return focusNum;
    }

    public void setFocusNum(Long focusNum) {

        this.focusNum = focusNum;
    }
}
