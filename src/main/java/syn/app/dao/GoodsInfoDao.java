package syn.app.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import syn.app.entity.GoodsInfo;

import java.util.List;
@Mapper
public interface GoodsInfoDao {
    @Select("select * from t_goods_info where product_id=#{id}")
    List<GoodsInfo> getGoodsInfoByPId(int id);
}
