package syn.app.dao;

import com.sun.xml.internal.ws.client.ClientSchemaValidationTube;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import syn.app.entity.Product;

import java.util.List;

@Mapper
public interface ProductDao {
    @Select("select * from t_product")
    List<Product> listProduct();


}
