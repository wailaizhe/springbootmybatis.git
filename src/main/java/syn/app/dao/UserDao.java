package syn.app.dao;


import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import syn.app.entity.User;

import java.util.List;

@Mapper
public interface UserDao {
    @Select("select * from t_user where username=#{name} ")
    User findUser(@Param("name") String name);
    @Select("select * from t_user  ")
    List<User> getAllUser();
    @Select("select * from t_user where id=#{id} ")
    User findById(@Param("id") int id);
}
