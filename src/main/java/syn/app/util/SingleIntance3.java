package syn.app.util;

/**
 * 单例-》静态内部类
 */
public class SingleIntance3 {
    private SingleIntance3(){}

    private static class IntaceHold{
        private static   SingleIntance3 instance=new SingleIntance3();
    }
    public SingleIntance3 getInstance(){

        return IntaceHold.instance;
    }
}
