package syn.app.util;

/**
 * 单例-》懒汉式，双重校验
 */
public class SingleIntance2 {
    private SingleIntance2(){}
    private  volatile SingleIntance2 instance;
    public SingleIntance2 getInstance(){
        if (instance==null){
            synchronized (SingleIntance2.class){
                instance=new SingleIntance2();
            }
        }
        return instance;
    }
}
