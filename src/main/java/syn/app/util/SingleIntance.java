package syn.app.util;

/**
 * 单例-》饿汉式
 */
public class SingleIntance {
    private SingleIntance(){}
    private final static SingleIntance instance=new SingleIntance();
    public SingleIntance getInstance(){
        return instance;
    }



}
