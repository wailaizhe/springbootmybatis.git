package syn.app.init;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class AskForLeave implements CommandLineRunner {

    @Override
    public void run(String... strings) throws Exception {
        createProductCache();
    }
    private void createProductCache(){
        System.out.println("初始化数据");
    }
}
