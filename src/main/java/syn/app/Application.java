package syn.app;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.Mapping;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import syn.app.filter.GlobalFilter;

/**
 * Created by Administrator on 2018\3\11 0011.
 */
@SpringBootApplication(scanBasePackages ={ "syn.app.*",})

public class Application  {


    public static void main(String[] args) {
        SpringApplication.run(Application.class,args);
    }
}
