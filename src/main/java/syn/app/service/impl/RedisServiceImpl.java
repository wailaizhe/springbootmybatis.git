package syn.app.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import syn.app.service.RedisService;
import syn.app.util.RedisUtil;


import java.util.List;
@Service
public class RedisServiceImpl implements RedisService {
    @Autowired
    private RedisUtil redisUtil;
    @Override
    public  boolean putKV(String key,Object o){
        return redisUtil.set(key,o);
    }
    @Override
    public boolean putList(String key, List list){
        return redisUtil.lSet(key,list);
    }
    @Override
    public Object getV(String key){
        return redisUtil.get(key);
    }
}
