package syn.app.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.BoundZSetOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.stereotype.Service;
import syn.app.dao.UserDao;
import syn.app.entity.User;
import syn.app.entity.UserScore;
import syn.app.service.UserService;
import syn.app.util.ConstantPrame;
import syn.app.util.RedisUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;
    @Autowired
    private RedisUtil redisUtil;
    private final String fold="focus:";
    @Override
    public User findUser(String name) {

        return userDao.findUser(name);
    }

    @Override
    public List<User> getAllUser() {
        List<User> allUser = userDao.getAllUser();
        for (User user : allUser) {
            List<Object> objects = redisUtil.lGet(ConstantPrame.THUMB + user.getId(), 0, -1);
            if (objects!=null){
                List<User> list=new ArrayList<>();
                objects.forEach(o->{list.add((User) o);});
                user.setThumbList(list);
            }

            user.setFocusNum(Long.parseLong(redisUtil.get(fold+user.getId())==null?"0":redisUtil.get(fold+user.getId()).toString()));
        }


        return allUser;
    }

    @Override
    public Long userFocusNum(User user) {
        Object o = redisUtil.get(fold + user.getId());
        return Long.parseLong(o.toString());
    }

    @Override
    public long focus(User user) {
        if (user.isFocus()){
             redisUtil.getRedisTemplate().boundZSetOps("sort").incrementScore(user.getUserName(), 1);
            return   redisUtil.incr(fold+user.getId(),1);
        }
        long decr = redisUtil.decr(fold + user.getId(), 1);
        Double sort = redisUtil.getRedisTemplate().boundZSetOps("sort").incrementScore(user.getUserName(), -1);
        if (decr<0){
            return   redisUtil.incr(fold+user.getId(),1);
        }
        if (sort<0){
             redisUtil.getRedisTemplate().boundZSetOps("sort").incrementScore(user.getUserName(), 1);
        }
        return decr;
    }

    @Override
    public User login(User user) {
        if (user==null) return null;
        if (user.getUserName().isEmpty()) return null;
        User user1 = userDao.findUser(user.getUserName());
        if (user1 ==null) return  null;
        int id = user1.getId();
        String key = ConstantPrame.FOLD + id;
        user1.setToken(key);
        user1.setPassword(null);
        redisUtil.set(key,user1,ConstantPrame.LIVE_TIME);
        return user1;
    }

    @Override
    public void thumb(User user, int id) {
        //User user2=userDao.findById(id);
        redisUtil.addToListRight(ConstantPrame.THUMB+id,user);
    }

    @Override
    public List<UserScore> getAllUserScore() {
        RedisTemplate redisTemplate = redisUtil.getRedisTemplate();
        Set sort = redisTemplate.opsForZSet().reverseRange("sort", 0, -1);
        UserScore us=null;
        List<UserScore> list=new ArrayList<>(sort.size());
        Object[] objects = sort.toArray();
        for (int i = 0; i < sort.size(); i++) {
            Object object = objects[i];
            us=new UserScore();
            us.setUserName(object.toString());
            us.setScore(redisTemplate.opsForZSet().score("sort",object));
            list.add(us);
        }

        return list;
    }
}
