package syn.app.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import syn.app.dao.GoodsInfoDao;
import syn.app.dao.ProductDao;
import syn.app.entity.GoodsInfo;
import syn.app.entity.Product;
import syn.app.service.GoodsInfoService;
import syn.app.util.ConstantPrame;
import syn.app.util.RedisUtil;

import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

@Service
public class GoodsInfoServiceImpl implements GoodsInfoService {
    @Autowired
    private ProductDao productDao ;
    @Autowired
    private GoodsInfoDao goodsInfoDao;
    @Autowired
    private RedisUtil redisUtil;
    @Override
    public List<Product> getProductList() {
        Map<Object, Object> hmget = redisUtil.hmget(ConstantPrame.PRODUCT);
        Product product2=null ;
        List<Product> products=null;
        if (hmget!=null && hmget.size()>0){
            Iterator<Map.Entry<Object, Object>> iterator = hmget.entrySet().iterator();
            products=new ArrayList<>(hmget.size());
            while (iterator.hasNext()){
                product2=new Product();
                product2.setProductName(iterator.next().getKey().toString());
                product2.setDesc(iterator.next().getValue().toString());
                products.add(product2);
            }
            return products;
        }
         products = productDao.listProduct();
        for (Product product : products) {

            List<GoodsInfo> goodsInfoByPId = goodsInfoDao.getGoodsInfoByPId(product.getId());
            product.setListGoods(goodsInfoByPId);
            product.setDesc(goodsInfoByPId.toString());
            redisUtil.hset(ConstantPrame.PRODUCT,product.getProductName(),goodsInfoByPId.toString());
        }
        return products;
    }
}
