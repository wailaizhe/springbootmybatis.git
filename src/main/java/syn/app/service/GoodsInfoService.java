package syn.app.service;

import syn.app.entity.Product;

import java.util.List;

public interface GoodsInfoService {
    List<Product> getProductList();
}
