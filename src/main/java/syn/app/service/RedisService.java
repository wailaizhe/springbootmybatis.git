package syn.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;


import java.util.List;

public interface RedisService {

    public  boolean putKV(String key,Object o);

    public boolean putList(String key, List list);

    public Object getV(String key);


}
