package syn.app.service;


import syn.app.entity.User;
import syn.app.entity.UserScore;

import java.util.List;
import java.util.Set;

/**
 * Created by Administrator on 2018\3\11 0011.
 */
public interface UserService {

    User findUser(String name);
    List<User> getAllUser();
    Long userFocusNum(User user);
    long focus(User user);
    User login(User user);
    void thumb(User user,int id);
    List<UserScore> getAllUserScore( );

}
