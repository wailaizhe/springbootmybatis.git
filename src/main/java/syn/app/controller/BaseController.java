package syn.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import syn.app.entity.User;
import syn.app.util.ConstantPrame;
import syn.app.util.RedisUtil;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class BaseController {

    @Autowired
    private RedisUtil redisUtil ;
    public User getCurrentUser(String token, HttpServletResponse response) throws IOException {
        if (StringUtils.isEmpty(token)) return null;
        Object o = redisUtil.get(token);
        if (o==null){
            response.sendRedirect("login");
        }
        if (StringUtils.isEmpty(o)) return null;
        return (User) o;
    }
}
