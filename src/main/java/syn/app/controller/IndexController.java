package syn.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import syn.app.entity.GoodsInfo;
import syn.app.entity.Product;
import syn.app.entity.User;
import syn.app.entity.UserScore;
import syn.app.service.GoodsInfoService;
import syn.app.service.UserService;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by Administrator on 2018\6\20 0020.
 */
@Controller
@RequestMapping("")
public class IndexController extends BaseController{
    @Autowired
    private UserService userService;
    @Autowired
    private GoodsInfoService goodsInfoService;

    @RequestMapping("/index")
    public String index2(Map<String, Object> map) {
        map.put("login","login");
        return "login";
    }
    @RequestMapping("/login")

    public String login(Map<String, Object> map) {

        return "login";
    }
    @RequestMapping("/login2")
    public String login2(HttpServletResponse response, Map<String, Object> map, User user) throws Exception{
        String returnPage="hello" ;
        if (user.getUserName().isEmpty()){
            map.put("errMsg","用户名不能为空");
           return "login";
        }
        if (user.getPassword().isEmpty()){
            map.put("errMsg","用户密码不能为空");
            return   "login";
        }
        User login = userService.login(user);
        if (login==null){
            map.put("errMsg","用户不存在");
            return   "login";
        }
        map.put("user",login);
        addInfo(map);
        response.sendRedirect("hello?token="+login.getToken());
        return "hello";
    }

    @RequestMapping("/hello")
    public String hello(HttpServletResponse response,Map<String, Object> map,String token) throws IOException {
        List<User> allUser = userService.getAllUser();
        map.put("users", allUser);
        User currentUser = getCurrentUser(token,response);
        map.put("user",currentUser);
        addInfo(map);
        return "hello";
    }

    @RequestMapping("/focus")
    public String focus(HttpServletResponse response, Map<String, Object> map,User user) throws IOException {

        System.out.println("关注成功"+user.getId());

        user.setFocus(true);
        user.setId(user.getId());
        userService.focus(user);
        addInfo(map);
        User currentUser = getCurrentUser(user.getToken(),response);
        map.put("user",currentUser);
        response.sendRedirect("hello?token="+currentUser.getToken());
        return "hello";
    }
    @RequestMapping("/cancel")
    public String cancel(HttpServletResponse response, Map<String, Object> map,User user) throws IOException {

        System.out.println("取消关注成功"+user.getId());
        user.setId(user.getId());
        user.setFocus(false);
        userService.focus(user);
        addInfo(map);
        User currentUser = getCurrentUser(user.getToken(),response);
        map.put("user",currentUser);
        response.sendRedirect("hello?token="+currentUser.getToken());
        return "hello";
    }

    @RequestMapping("/thumb")
    public String thumb(HttpServletResponse response, Map<String, Object> map,int id,String token) throws IOException {
        User currentUser = getCurrentUser(token,response);

        User user=new User();
        user.setId(id);
        user.setFocus(false);
        userService.focus(user);
        userService.thumb(currentUser,id);
        map.put("user",currentUser);
        addInfo(map);
        response.sendRedirect("hello?token="+currentUser.getToken());
        return "hello";
    }

    @RequestMapping("/exception")
    public void exception() throws Exception {
        throw new Exception();
    }
    private void addInfo(Map map){
        List<User> allUser = userService.getAllUser();
        map.put("users", allUser);
        List<UserScore> userScores = userService.getAllUserScore();
        map.put("userScores",userScores);
        List<Product> productList = goodsInfoService.getProductList();
        map.put("products",productList);

    }
}