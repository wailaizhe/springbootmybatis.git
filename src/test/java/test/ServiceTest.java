package test;




import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import syn.app.Application;
import syn.app.entity.User;
import syn.app.service.RedisService;
import syn.app.util.ConstantPrame;
import syn.app.util.RedisUtil;

import java.util.*;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class ServiceTest {
    @Autowired
    private RedisService redisService ;
    @Autowired
    private RedisUtil redisUtil;
    @Autowired
    private RedisTemplate redisTemplate;
    @Test
    public void pustTetst (){
        redisService.putKV("key","111");
        System.out.println(redisService.getV("key"));
    }
    @Test
    public void testList(){
        List list=new ArrayList();
        list.add("1");
        list.add("2");
        list.add("3");
       // redisUtil.del("wangwu");
        redisUtil.lSet("wangwu",list);

        List<Object> lisi = redisUtil.lGet("wangwu", 0, -1);
        for (Object o : lisi) {
            System.out.println(o);
        }
    }
    @Test
    public void addRight(){
        redisUtil.addToListRight("wangwu","4");
        List<Object> lisi = redisUtil.lGet("wangwu", 0, -1);
        for (Object o : lisi) {
            System.out.println(o);
        }
    }
    @Test
    public void putObject(){
        User user=new User();
        user.setId(1);
        user.setUserName("li");
       // user.setPassWord("123");
        redisUtil.set(user.getUserName(),user);
        System.out.println(redisUtil.get(user.getUserName()));
    }
    @Test
    public void del(){
        Set wukong = redisUtil.keys("login:*");
        for (Object o : wukong) {
            System.out.println(o);
            redisUtil.del(o.toString());
        }
    }
    @Test
    public void remove(){
        //long wangwu = redisUtil.lRemove("wangwu", 1, "1");
        //System.out.println(wangwu);
        List<Object> lisi = redisUtil.lGet("wangwu", 0, -1);
        for (Object o : lisi) {
            System.out.println(o);
        }
    }
    @Test
    public void redisTest(){

        redisUtil.hset("person","shou",21);
        redisUtil.hset("person","sex","男");
        redisUtil.hset("person","home","北京");
        redisUtil.hset("person","home","大兴");
        Map<Object,Object> map = redisUtil.hmget("person");
        Iterator<Map.Entry<Object, Object>> iterator = map.entrySet().iterator();
        while (iterator.hasNext()){
            System.out.println(iterator.next());
            System.out.println(iterator.next().getValue());
        }




    }
    @Test
    public void redisTestSort(){
        redisTemplate.opsForZSet().add("sort","李四",1);
        redisTemplate.opsForZSet().add("sort","王五",3);
        redisTemplate.opsForZSet().add("sort","赵六",5);
        redisTemplate.opsForZSet().add("sort","周七",2);

        Set sort = redisTemplate.opsForZSet().range("sort", 0, -1);
        sort.forEach(s->{
            Double sort1 = redisTemplate.boundZSetOps("sort").score(s);
            System.out.println("person:"+s+",score:"+sort1);
        });


    }
    @Test
    public void testZset(){
        redisUtil.getRedisTemplate().boundZSetOps("sort").incrementScore("王五",1);
        Double score = redisTemplate.boundZSetOps("sort").score("王五");
        System.out.println(score);
    }
    @Test
    public void testBitMap(){
        //login.20180906 102400000 0
        String key="login:";
        Long today=20200525L;
        for (int i = 0; i < 1000; i++) {
            if (i%2==0){
                redisTemplate.opsForValue().setBit(key+(today+i),i,true);
            }else{
                redisTemplate.opsForValue().setBit(key+(today+i),i,false);
            }

        }

    }

}
